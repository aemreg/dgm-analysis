# DGM Analysis



## Introduction

This repository contains codes from the thesis on analyzing Deep Generative Models. 
Data Set generation notebook contains the relevant data generations and transformations that are used in this study.
NVP and GAN notebooks contains the model architectures and training steps used in this study. 
Divergence estimation contains the estimated KL metric and MDL metric implementations and methods for comparing the models at hand for this study.
Trained models are contained as compressed files due to their high storage. 

All trained models can be accessed from https://drive.google.com/file/d/1MFpa0szhDGB2Rkq712qZw7q9Dy4we1QA/view?usp=sharing
## License
Anyone can benefit and contribute to this project.
